const items = require("./arrays-vitamins.js")

//1. Get all items that are available 

function itemsAvailable(items) {

    const itemsAvailable = items.map(eachVitamin => eachVitamin)
    console.log(itemsAvailable)
    console.log('\n')

}

itemsAvailable(items)


//2. Get all items containing only Vitamin C.

function onlyVitaminC(items) {

    const vitaminC = items.filter(eachVitamin => {
        if (eachVitamin.contains === "Vitamin C") {

            return true
        
        }
        else {

            return false
        
        }
    })
    console.log(vitaminC)
    console.log('\n')
}

onlyVitaminC(items)


//3. Get all items containing Vitamin A.

function vitaminA(items) {

    const vitaminA = items.filter(eachVitamin => {

        if (eachVitamin.contains.includes("Vitamin A") || eachVitamin.contains === "Vitamin A") {

            return true

        }
        else {

            return false

        }
    })
    console.log(vitaminA)
    console.log('\n')
}

vitaminA(items)


/*4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.*/

function groupVitamins(items) {

    const groupVitamins = items.reduce((acc, curr) => {
    
        let vitamins = []
        if (acc[curr.contains]) {

            acc[curr.contains].push(curr["name"])

        } else {

            vitamins = curr.contains.split(",")
            if (vitamins.length === 2) {

                vitamins.map(each => {

                    each = each.trim()
                    if (acc[each]) {

                        acc[each].push(curr.name)

                    }
                    else {

                        acc[each] = [curr.name]

                    }

                })
            }
            else {

                acc[curr.contains] = [curr.name]

            }

        }
        return acc
    }, {})

    console.log(groupVitamins)
    console.log("\n")
}

groupVitamins(items)


//5. Sort items based on number of Vitamins they contain.

function sortedItems(items) {
    const sortedItems = items.sort((a, b) => {

        if (a.contains.length < b.contains.length) return 1
        else if (a.contains.length > b.contains.length) return -1
        else return 0
        
    })
    console.log(sortedItems)
}

sortedItems(items)



